package project.senseIt.start;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class ShakeTester {

	SensorManager mySensorManager; 
	SensorEventListener sEventListener;
	Activity activity;
	Map<Integer, Integer> map;
	private long lastUpdate;
	private int color = 0;
	public int Progress=0;
	Button b=null, b2=null;
	
	public ShakeTester()
	{
		activity=SenseItTrial.activity;
		Animation anim=AnimationUtils.loadAnimation(activity.getApplicationContext()
				,R.anim.anim);
        activity.getWindow().getDecorView().startAnimation(anim);
		activity.setContentView(R.layout.shake);
		final TextView desc = (TextView)activity.findViewById(R.id.desc);
		desc.setText("Set SenseItTrial.shakeSensitivity of Shake for your Phone");
		SwipeGesture swipe = new SwipeGesture(activity.getApplicationContext());
		final RelativeLayout layout=(RelativeLayout) activity.findViewById(R.id.shakeLayout);
        layout.setOnTouchListener(swipe);
        
        final SeekBar shake = (SeekBar)activity.findViewById(R.id.shakeBar);
        shake.setProgress((int)((double)(100*SenseItTrial.shakeSensitivity)/12.5));
        shake.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
          
            public void onStopTrackingTouch(SeekBar arg0) {
            	SenseItTrial.shakeSensitivity=1.0+(double)(Progress*11.5)/100;
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onProgressChanged(SeekBar bar, int val, boolean arg2) {
                shake.setProgress(val);
                Progress=val;
            }
        });
        
        map = new HashMap<Integer, Integer>();
        map.put(0, R.drawable.bg10);
        map.put(1, R.drawable.bg16);
        map.put(2, R.drawable.bg3);
        map.put(3, R.drawable.bg4);
        map.put(4, R.drawable.bg5);
        map.put(5, R.drawable.bg13);
        map.put(6, R.drawable.bg8);
        map.put(7, R.drawable.bg12);
        map.put(8, R.drawable.bg9);
        map.put(9, R.drawable.bg15);
        
        final Intent nextScreen = new Intent(activity, SenseItService.class);
        
        b=(Button)activity.findViewById(R.id.shakebutton2);
	    b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
            	activity.stopService(nextScreen);
            	activity.finish();
            	System.exit(0);
            }//end onClick
	    });//end new View.onClickListener
	    
	    
	    b2=(Button)activity.findViewById(R.id.shakebutton1);
	    b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
				activity.startService(nextScreen);
				activity.finish();
            }//end onClick
	    });//end new View.onClickListener

        
        mySensorManager = (SensorManager)activity.getSystemService(Context.SENSOR_SERVICE);
        
        sEventListener = new SensorEventListener(){
        	@Override
        	public void onAccuracyChanged(Sensor sensor, int accuracy) {
        		// TODO Auto-generated method stub
        	}
        	
        	private void getAccelerometer(SensorEvent event) {
    		    float[] values = event.values;
    		    // Movement
    		    float x = values[0];
    		    float y = values[1];
    		    float z = values[2];

    		    float accelationSquareRoot = (x * x + y * y + z * z)
    		        / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
    		    long actualTime = System.currentTimeMillis();
    		    if (accelationSquareRoot >= SenseItTrial.shakeSensitivity)
    		    {
    		      if (actualTime - lastUpdate < 500)
    		      {
    		        return;
    		      }
    		      lastUpdate = actualTime;
    		      //Toast.makeText(this, "Device was shuffed", Toast.LENGTH_SHORT).show();
    		      if(Float.floatToIntBits(x)>8)
    		      {	  if(color==0)
    		    		  color=map.size();
    		    	  color = (color-1)%map.size();
    		      }
    		    	  else
    		    	  color = (color+1)%map.size();
    		      layout.setBackgroundResource(map.get(color));
    		    }
    		  }

        	
        	protected void onResume() {
            	mySensorManager.registerListener(this, mySensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), mySensorManager.SENSOR_DELAY_GAME);
        	    mySensorManager.registerListener(this, mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),mySensorManager.SENSOR_DELAY_UI);
        	}
        	
        	
        	@Override
        	public void onSensorChanged(SensorEvent event) {
        		
        		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
        	      getAccelerometer(event);
        	    }
        	}

        };
        
        mySensorManager.registerListener(sEventListener, mySensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), mySensorManager.SENSOR_DELAY_GAME);
    	mySensorManager.registerListener(sEventListener, mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),mySensorManager.SENSOR_DELAY_NORMAL);
	}
}
