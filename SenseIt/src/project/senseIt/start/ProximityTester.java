package project.senseIt.start;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class ProximityTester {

	SensorEventListener proximitySensorEventListener;
	SensorManager mySensorManager;
	Sensor myProximitySensor;
	Activity activity;
	ImageView image;
	Animation zoom;
	Button b=null, b2 = null;
	
	public ProximityTester()
	{
		activity=SenseItTrial.activity;
		Animation anim=AnimationUtils.loadAnimation(activity.getApplicationContext()
				,R.anim.anim);
        activity.getWindow().getDecorView().startAnimation(anim);
		activity.setContentView(R.layout.proximity);
		final TextView desc = (TextView)activity.findViewById(R.id.desc);
		desc.setText("Test Proximity Sensor of your phone");
		SwipeGesture swipe = new SwipeGesture(activity.getApplicationContext());
        (activity.findViewById(R.id.proximityLayout)).setOnTouchListener(swipe);
        zoom = AnimationUtils.loadAnimation(activity.getBaseContext(),R.anim.zoom_in);
		zoom.setStartOffset(0);
		image=(ImageView)activity.findViewById(R.id.back);
        mySensorManager = (SensorManager)activity.getSystemService(Context.SENSOR_SERVICE);
        myProximitySensor = mySensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        
        final Intent nextScreen = new Intent(activity, SenseItService.class);
        
        b=(Button)activity.findViewById(R.id.proxybutton2);
	    b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
            	activity.stopService(nextScreen);
            	activity.finish();
            	System.exit(0);
            }//end onClick
	    });//end new View.onClickListener
	    
	    
	    b2=(Button)activity.findViewById(R.id.proxybutton1);
	    b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
				activity.startService(nextScreen);
				activity.finish();
            }//end onClick
	    });//end new View.onClickListener
        
        proximitySensorEventListener = new SensorEventListener(){
        	@Override
        	public void onAccuracyChanged(Sensor sensor, int accuracy) {
        		// TODO Auto-generated method stub
        	}
        	
        	protected void onResume() {
        		mySensorManager.registerListener(this, myProximitySensor,SensorManager.SENSOR_DELAY_NORMAL);
        	  }
        	
        	
        	@Override
        	public void onSensorChanged(SensorEvent event) {
        		// TODO Auto-generated method stub

        		if(event.sensor.getType()==Sensor.TYPE_PROXIMITY)
        		{
        			if(String.valueOf(event.values[0]).equals("0.0"))
        			{
        				desc.setText("Proximity Sensor Reading: "+ "Near");
        				image.startAnimation(zoom);
        			}
        			else
        			{
        				desc.setText("Proximity Sensor Reading: "+ "Far");
        				image.clearAnimation();
        			}
        		}  		
        		
        	}

        };
        
        if(myProximitySensor == null)
        {
        	desc.setText("No Proximity Sensor!"); 
        }
        else
        {
        	//ProximitySensor.setText(myProximitySensor.getName());
        	//ProximityMax.setText("Maximum Range: "+ String.valueOf(myProximitySensor.getMaximumRange()));
        	mySensorManager.registerListener(proximitySensorEventListener,myProximitySensor,SensorManager.SENSOR_DELAY_NORMAL);
        }
        
	}
}
