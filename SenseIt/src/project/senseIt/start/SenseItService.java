package project.senseIt.start;

import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.Settings.SettingNotFoundException;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class SenseItService extends Service {
	  private Looper mServiceLooper;
	  private ServiceHandler mServiceHandler;

	  SensorEventListener sensorEventListener;
	  
	  SensorManager mySensorManager;
	  Sensor myProximitySensor;
	  Sensor mAccelerometer=null;
	  Sensor mMagnetometer=null;
	  Sensor mLight=null;
	  AudioManager mAudioManager;
	  LocationManager locationManager;
	  LocationListener locationListener;
	  MediaRecorder recorder=null;
	  
	  HandlerThread thread;	  
	  
	  private long lastUpdate;
	  private long lastProxUpdate;
	  private float mLastX, mLastY, mLastZ;
	  private boolean mInitialized;
	  private final float NOISE = (float) 2.0;
	  int ringVolume = 0;
	  int musicVolume = 0;
	  private float currentLux, maxLux;
	  
	  String mode = new String("Stationary");
	  String drive = SenseItTrial.drive;
	  boolean setSilent = false;
	  
	  // Handler that receives messages from the thread
	  private final class ServiceHandler extends Handler
	  {
	      public ServiceHandler(Looper looper)
	      {
	          super(looper);
	      }//end Servicehandler
	      
	      @Override
	      public void handleMessage(Message msg)
	      {
	          // Normally we would do some work here, like download a file.
	          // For our sample, we just sleep for 5 seconds.
	    	  //Toast.makeText(getApplicationContext(), "Before Sleep", Toast.LENGTH_SHORT).show();
	    	  drive = SenseItTrial.drive;
	          long endTime = System.currentTimeMillis() + 5*1000;
	          while (System.currentTimeMillis() < endTime)
	          {
	              synchronized(this)
	              {   try
	                  {
	                      wait(endTime - System.currentTimeMillis());
	                  }
	                  catch (Exception e)
	                  {
	                  }//end catch
	              }//end synchronised
	          }//end while
	          //Toast.makeText(getApplicationContext(), "After Sleep", Toast.LENGTH_SHORT).show();
	          // Stop the service using the startId, so that we don't stop
	          // the service in the middle of handling another job
	          /*Intent i = new Intent();
			  i.setClass(this, MyActivity.class);
			  i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			  startActivity(i);*/
	          //Intent intent = new Intent(senseItService.this,senseItActivity3.class);
	          //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	          //startActivity(intent);
	          
	          lastUpdate = System.currentTimeMillis();
	          lastProxUpdate = System.currentTimeMillis();
	          
	          mySensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
	          myProximitySensor = mySensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
	          mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
	          mAccelerometer = mySensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	          mMagnetometer = mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	          mLight = mySensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
	          
	          sensorEventListener = new SensorEventListener(){
	            	@Override
	            	public void onAccuracyChanged(Sensor sensor, int accuracy)
	            	{
	            		
	            	}//end onAccuracyChanged
	            	
	            	private void getAccelerometerMusic(SensorEvent event)
	            	{
	        		    float[] values = event.values;
	        		    // Movement
	        		    float x = values[0];
	        		    float y = values[1];
	        		    float z = values[2];

	        		    float accelationSquareRoot = (x * x + y * y + z * z)
	        		        / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
	        		    long actualTime = System.currentTimeMillis();
	        		    if(accelationSquareRoot >= 2.5)
	        		    {
	        		    	if (actualTime - lastUpdate < 500)
	        		    	{
	        		    		return;
	        		    	}//end if
	        		    	lastUpdate = actualTime;
	        		    	//Toast.makeText(this, "Device was shuffed", Toast.LENGTH_SHORT).show();
	        		    	if(Float.floatToIntBits(x)>12)
	        		    	{	
	        		    		Toast.makeText(getApplicationContext(), "Next Song", Toast.LENGTH_SHORT).show();
	        		    		Intent musicIntent = new Intent("com.android.music.musicservicecommand");
	        		    		musicIntent.putExtra("command", "next");
	        		    		sendBroadcast(musicIntent);
	        		    	}//end if
	        		    	else
	        		    	{
	        		    		Toast.makeText(getApplicationContext(), "Previous Song", Toast.LENGTH_SHORT).show();
	        		    		Intent musicIntent = new Intent("com.android.music.musicservicecommand");
	        		    		musicIntent.putExtra("command", "previous");
	        		    		sendBroadcast(musicIntent);
	        		    		musicIntent.putExtra("command", "previous");
	        		    		sendBroadcast(musicIntent);
	        		    	}
	        		    }//endIf
	        		  }//endGetAccelerometer

	            	private void getAccelerometerOther(SensorEvent event)
	            	{
	        		    float[] values = event.values;
	        		    // Movement
	        		    float x = values[0];
	        		    float y = values[1];
	        		    float z = values[2];

	        		    float accelationSquareRoot = (x * x + y * y + z * z)
	        		        / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
	        		    long actualTime = System.currentTimeMillis();
	        		    if(accelationSquareRoot >= 2.5)
	        		    {
	        		    	if (actualTime - lastUpdate < 500)
	        		    	{
	        		    		return;
	        		    	}//end if
	        		    	lastUpdate = actualTime;
	        		    	//Toast.makeText(this, "Device was shuffed", Toast.LENGTH_SHORT).show();
	        		    	if(Float.floatToIntBits(x)>12)
	        		    	{	
	        		    		Toast.makeText(getApplicationContext(), "Switching sound profile", Toast.LENGTH_SHORT).show();
	        		    		//int currentRingerMode = ((AudioManager) getSystemService(AUDIO_SERVICE)).getRingerMode();
	        		    		if(mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE)
	        		    		{
	        		    			mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
	        		    			setSilent = false;
	        		    			Intent startMain = new Intent(Intent.ACTION_MAIN);
	        		    	        startMain.addCategory(Intent.CATEGORY_HOME);
	        		    	        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        		    	        startActivity(startMain);
	        		    		}
	        		    		else
	        		    		{
	        		    			mAudioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
	        		    			setSilent = true;
	        		    		}
	        		    	}//end if
	        		    	else
	        		    	{
	        		    		Toast.makeText(getApplicationContext(), "Switching sound profile", Toast.LENGTH_SHORT).show();
	        		    		if(mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE)
	        		    		{
	        		    			mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
	        		    		}
	        		    		else
	        		    		{
	        		    			mAudioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
	        		    		}
	        		    	}
	        		    }//endIf
	        		  }//endGetAccelerometer

	            	
	            	private void musicToggleOnProximity(SensorEvent event)
	            	{
	            		long actualTime = System.currentTimeMillis();
	            		if(String.valueOf(event.values[0]).equals("0.0"))
	            		{
	            			lastProxUpdate = actualTime;
	            			Toast.makeText(getApplicationContext(), "Near", Toast.LENGTH_SHORT).show();
	            		}//end if(near)
	            		else
	            		{
	            			Toast.makeText(getApplicationContext(), "Far", Toast.LENGTH_SHORT).show();
	            			if (actualTime - lastProxUpdate > 1500)
	        		    	{
	            				if(!mAudioManager.isMusicActive())
	            				{
	            					 Toast.makeText(getApplicationContext(), "Long", Toast.LENGTH_SHORT).show();
	            					 Intent intent = new Intent("android.intent.action.MUSIC_PLAYER");
	            			         intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	            			         startActivity(intent);
	            			         Intent musicIntent = new Intent("com.android.music.musicservicecommand");
			            			 musicIntent.setAction("com.android.music.musicservicecommand.togglepause");
			            			 musicIntent.putExtra("command", "togglepause");
			            			 //sendBroadcast(musicIntent);
			            			 //sendBroadcast(musicIntent);
	            				}
	            				else
	            				{
	            					Toast.makeText(getApplicationContext(), "Longer", Toast.LENGTH_SHORT).show();
	            					Intent musicIntent = new Intent("com.android.music.musicservicecommand");
		            				musicIntent.setAction("com.android.music.musicservicecommand.togglepause");
		            				musicIntent.putExtra("command", "togglepause");
		            				//sendBroadcast(musicIntent);
	            				}
	            				Toast.makeText(getApplicationContext(), "Music toggle", Toast.LENGTH_SHORT).show();
	        		    	}//end if(delay)
	        		    	//lastProxUpdate = actualTime;
	            		}//end else(if(near))
	            	}//end musicToggleOnProximity
	            	
	            	@Override
	            	public void onSensorChanged(SensorEvent event)
	            	{
	            		if (mAudioManager.isMusicActive())
	            		{
	            			if(event.sensor.getType()==Sensor.TYPE_PROXIMITY)
		            		{
		            			if(String.valueOf(event.values[0]).equals("0.0"))
		            			{
		            				//Toast.makeText(getApplicationContext(), "Proximity Changed to Near", Toast.LENGTH_SHORT).show();
		            			}//end if(near)
		            			else
		            			{
		            				//Toast.makeText(getApplicationContext(), "Proximity Changed to Far", Toast.LENGTH_SHORT).show();
		            			}//end else(if(near))
		            			musicToggleOnProximity(event);
		            		}//end if(proximity)
		            		if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
		            		{
		            	      getAccelerometerMusic(event);
		            	    }//end if(accelerometer)
	            		}//end if(mAudioManager.isMusicActive())
	            		else if(!mAudioManager.isMusicActive())
	            		{
	            			if(event.sensor.getType()==Sensor.TYPE_PROXIMITY)
		            		{
		            			if(String.valueOf(event.values[0]).equals("0.0"))
		            			{
		            				//Toast.makeText(getApplicationContext(), "Music Off... Proximity Changed to Near. No action.", Toast.LENGTH_SHORT).show();
		            			}//end if(near)
		            			else
		            			{
		            				//Toast.makeText(getApplicationContext(), "Music Off... Proximity Changed to Far. No action.", Toast.LENGTH_SHORT).show();
		            			}//end else(if(near))
		            			musicToggleOnProximity(event);
		            		}//end if(proximity)
	            			if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
		            		{
		            	      getAccelerometerOther(event);
		            	    }//end if(accelerometer)
	            		}//end else(if(mAudioManager.isMusicActive()))
	            		if(event.sensor.getType()== Sensor.TYPE_LIGHT)
	            	    {
	            			float curBrightnessValue = 320;
	            			try
	            			{
	                    	    curBrightnessValue = android.provider.Settings.System.getInt(
	                    	    getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS);
	                    	    Toast.makeText(getApplicationContext(), "brightness: "+curBrightnessValue, 
	                    	    			   Toast.LENGTH_LONG).show();
	                    	}
	            			catch (SettingNotFoundException e)
	            			{
	                    	    // TODO Auto-generated catch block
	                    	    e.printStackTrace();
	                    	}
	            	        currentLux = event.values[0];
	            	        //if(currentLux>50)
	            	        	//Toast.makeText(getApplicationContext(), "lux: "+currentLux, Toast.LENGTH_LONG).show();
	            	        //if (currentLux > maxLux)
	            	           //maxLux = currentLux;
	            	        float newBrightnessValue = curBrightnessValue + ((320 - currentLux)/10);
	            	        if(newBrightnessValue>255)
	            	        	newBrightnessValue=255;
	            	        else if(newBrightnessValue<30)
	            	        	newBrightnessValue=30;
	            	        android.provider.Settings.System.putInt(getContentResolver(),
	                    			android.provider.Settings.System. SCREEN_BRIGHTNESS,   (int)(newBrightnessValue));
	            	        //light.setText("Sensed Light change : "+currentLux);
	            	    }//end if
	            		
	            		synchronized (this)
	            	    {
	            	        switch(event.sensor.getType())
	            	        {
	            	            case Sensor.TYPE_ACCELEROMETER:
	            	            {
	            	            	float x = event.values[0];
	            	            	float y = event.values[1];
	            	            	float z = event.values[2];
	            	            	if(!mInitialized)
	            	            	{
	            	            		mLastX = x;
	            	            		mLastY = y;
	            	            		mLastZ = z;
	            	            		mInitialized = true;
	            	            	}// end if
	            	            	else
	            	            	{
	            	            		float deltaX = Math.abs(mLastX - x);
	            	            		float deltaY = Math.abs(mLastY - y);
	            	            		float deltaZ = Math.abs(mLastZ - z);
	            	            		if(deltaX < NOISE)
	            	            			deltaX = (float)0.0;
	            	            		if(deltaY < NOISE)
	            	            			deltaY = (float)0.0;
	            	            		if(deltaZ < NOISE)
	            	            			deltaZ = (float)0.0;
	            	            		mLastX = x;
	            	            		mLastY = y;
	            	            		mLastZ = z;
	            	            		
	            	            	}// end else
	            	            	break;
	            	            }// end case
	            	            case Sensor.TYPE_MAGNETIC_FIELD:
	            	            	//outputX2.setText("x:"+Float.toString(event.values[0]));
	            	                //outputY2.setText("y:"+Float.toString(event.values[1]));
	            	                //outputZ2.setText("z:"+Float.toString(event.values[2]));
	            	            	break;
	            	        }//endSwitch
	            	    }//endSychronised
	            		
	            	}//endOnSensorChanged
	            };//endSensorEventListener

	            if(myProximitySensor == null)
	            {
	            	Toast.makeText(getApplicationContext(), "NULL", Toast.LENGTH_SHORT).show();
	            }//end if
	            else
	            {
	            	mySensorManager.registerListener(sensorEventListener,myProximitySensor,SensorManager.SENSOR_DELAY_NORMAL);
	            	mySensorManager.registerListener(sensorEventListener, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	                mySensorManager.registerListener(sensorEventListener, mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
	                mySensorManager.registerListener(sensorEventListener, mLight, SensorManager.SENSOR_DELAY_NORMAL);
	            }//end else
	          
	  	        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	  	        locationListener = new LocationListener()
	  	        {
	  	        	Location prevLoc=null;
	  	        	public void onLocationChanged(Location location)
	  	        	{
	  	        		// Called when a new location is found by the network location provider. 								
	  	        		if(location.hasSpeed())
	  	        		{
	  	        			if(location.getSpeed()>38.0 && drive.equals("On"))
	  	        			{
	  	        				mode = new String("Driving");
	  	        			}
	  	        			else
	  	        			{
	  	        				mode = new String("Stationary");
	  	        			}
	  	        			//Toast.makeText(getApplicationContext(), "\nCurrent speed: "+1.6*location.getSpeed()+" km/h", Toast.LENGTH_SHORT).show();
	  	        			//tv.setText("\nCurrent speed: " + location.getSpeed() + " km/h");
	  	        		}//end if
	  	        		else
	  	        		{
	  	        			//Toast.makeText(getApplicationContext(), "\nNo speed", Toast.LENGTH_SHORT).show();
	  	        			mode = new String("Stationary");
	  	        		}//end else if(location.hasSpeed())
	  	        		if(prevLoc==null)
	  	        		{
	  	        			prevLoc=location;
	  	        			//tv.append("\n"+location.toString());
	  	        		}//end if
	  	        		else
	  	        		{
	  	        		}//end else
	  	        	}//end onLocationChanged
	  	        	public void onStatusChanged(String provider, int status, Bundle extras)
	  	        	{
	  	        		//tv.append("\n"+provider);
	  	        	}//end onStatusChanged
	  	        	
	  	        	public void onProviderEnabled(String provider)
	  	        	{
	  	        		//tv.append("\nProvider Enabled : "+provider);
	  	        	}//end onProviderEnabled
	  	        	
	  	        	public void onProviderDisabled(String provider)
	  	        	{
	  	        		//tv.append("\nProvider Disabled");
	  	        	}//end onProviderDisabled
	  	        };//end locationlistener = new LocationListener()
	  	        
	  	        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
	            
	  	        //Intent intent = new Intent("com.android.music"); startActivity(intent);
	            //Intent intent = new Intent("android.intent.action.MUSIC_PLAYER");
	            //intent.setAction(Intent.ACTION_MAIN);
                //intent.addCategory(Intent.CATEGORY_LAUNCHER);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	            //startActivity(intent);
	            //Intent intent = newfsense Intent("MediaStore.INTENT_ACTION_MUSIC_PLAYER"); startActivity(intent);
	            //MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext());
	            //mediaPlayer.start();
	            /*Intent musicIntent = new Intent();
	            musicIntent.setClassName("com.android.music.musicservicecommand", "com.google.android.music.MusicPlaybackService");
				musicIntent.setAction("com.android.music.musicservicecommand.togglepause");
				musicIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				musicIntent.putExtra("command", "togglepause");
				sendBroadcast(musicIntent);*/
	            
				//Toast.makeText(getApplicationContext(), "MUSIC..", Toast.LENGTH_SHORT).show();
	            IntentFilter inF=new IntentFilter();
	  	        inF.addAction("android.intent.action.PHONE_STATE");
	  	        inF.addAction("android.intent.action.NEW_OUTGOING_CALL");
	  	        rcvr r=new rcvr();
	  	        registerReceiver(r,inF);
	          
	  	        ringVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_RING);
	  	        musicVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
	  	        recorder = new MediaRecorder();
	  	        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
	  	        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
	  	        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
	  	        recorder.setOutputFile("/dev/null");
	  	        
	  	        try
	  	        {
	  	        	recorder.prepare();
	  	        	recorder.start();
	  	        }
	  	        catch (Exception e)
	  	        {
	  	        	Toast.makeText(getApplicationContext(), "\n"+e.getMessage(), Toast.LENGTH_SHORT).show();
	  	        	//audio.setText(e.getMessage());
	  	        }//end catch
	  	        try
	  	        {
	  	        	Timer t=new Timer();
	  	        	t.scheduleAtFixedRate(new AudioLevel(), 0, 2000);
	  	        }//end try
	  	        catch(Exception e)
	  	        {
	  	        	Toast.makeText(getApplicationContext(), "\n"+e.getMessage(), Toast.LENGTH_SHORT).show();
	  	        	//audio.setText(e.getMessage());
	  	        }//end catch
	          //timer.scheduleAtFixedRate(new RecorderTask(recorder), 0, 1000);
	  	        
	          //stopSelf(msg.arg1);
	          //mServiceLooper.quit();
	      }//end handleMessage
	      
	      
	      class rcvr extends BroadcastReceiver
	      {
	    	  private ITelephony telephonyService;
	    	  boolean flag = true;
	    	  @Override
	    	  public void onReceive(Context context, Intent intent) 
	    	  {
	    		  //boolean flag = true;
	    		  String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
	    		  //tv.append("\nIncoming call from :"+number);
	    		  if(number.equals("+919868127990") || mode.equals("Stationary"))
	    			  return;
	    		  TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	    		  try
	    		  {
	    			  Class c = Class.forName(telephony.getClass().getName());
	    			  Method m = c.getDeclaredMethod("getITelephony");
	    			  m.setAccessible(true);
	    			  telephonyService = (ITelephony) m.invoke(telephony);
	    			  telephonyService.silenceRinger();
	    			  telephonyService.endCall();
	    		  }//end try
	    		  catch(Exception e)
	    		  {
	    			  //tv.append(e.getMessage());
	    		  }//end catch
	    		  //tv.append("\nCall Terminated" + number);
	    		  try
	    		  {
	    			  SmsManager smsManager = SmsManager.getDefault();
	    			  if(flag)
	    				  smsManager.sendTextMessage(number, null, "I am driving.\n" +"I will call you Later!", null, null);
	    		  }//end try
	    		  catch(Exception e)
	    		  {
	    			  //tv.append(e.getMessage());
	    		  }//end catch
	    		  flag=!flag;
	    	  }//end onReceive
	      };//end class rcvr
	      
	      class AudioLevel extends TimerTask
	      {
	    	  public void run()
	  		  {
	    		  double noiseDb = 0.0;
	    		  if(recorder!=null)
	    		  {
	    			  noiseDb = 20*Math.log10(recorder.getMaxAmplitude()/7.5);
	    			  int maxRingVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
	    			  int maxMusicVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
	    			  int newRingVolume = maxRingVolume;
	    			  int oldRingVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_RING);
	    			  int newMusicVolume = maxMusicVolume;
	    			  int oldMusicVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
	    			  //int minRingVolume = mAudioManager.getStreamMinVolume(AudioManager.STREAM_RING);
	    			  //int minMusicVolume = mAudioManager.getStreamMinVolume(AudioManager.STREAM_MUSIC);
	    			  newRingVolume = oldRingVolume + ((int)(noiseDb-70))/20;
	    			  if(newRingVolume>maxRingVolume)
	    				  newRingVolume = maxRingVolume;
	    			  else if(newRingVolume<0)
	    				  newRingVolume = 0;
	    			  newMusicVolume = oldMusicVolume + ((int)(noiseDb-70))/10;
	    			  if(newMusicVolume>maxMusicVolume)
	    				  newMusicVolume = maxMusicVolume;
	    			  else if(newMusicVolume<0)
	    				  newMusicVolume = 0;
	    			  mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxMusicVolume, AudioManager.FLAG_ALLOW_RINGER_MODES);
	    			  if(setSilent)
	    			  {
	    				  
	    			  }
	    			  else if(noiseDb>0)
	    			  {
	    				mAudioManager.setStreamVolume(AudioManager.STREAM_RING, maxRingVolume, AudioManager.FLAG_ALLOW_RINGER_MODES);
		    			//Toast.makeText(getApplicationContext(), "Noise Level(in db) : "+noiseDb, Toast.LENGTH_SHORT).show();
	    			  }
	    			  //else if(noiseDb<=10)
	    			  //{
	    				//mAudioManager.setStreamVolume(AudioManager.STREAM_RING, int (noiseDb) , AudioManager.FLAG_ALLOW_RINGER_MODES);
		    			//Toast.makeText(getApplicationContext(), "Noise Level(in db) : "+noiseDb, Toast.LENGTH_SHORT).show();
	    			  //}
	    				  //mAudioManager.setStreamVolume(AudioManager.STREAM_RING, maxRingVolume, AudioManager.FLAG_ALLOW_RINGER_MODES);
	    			  //Toast.makeText(getApplicationContext(), "Noise Level(in db) : "+noiseDb, Toast.LENGTH_SHORT).show();
	    		  }
	    		  else
	    		  {
	    			  //Toast.makeText(getApplicationContext(), "Audio Recorder NULL", Toast.LENGTH_SHORT).show();  
	    		  }
	    		  //post("Noise Level(in db) : "+20*Math.log10(recorder.getMaxAmplitude()/7.5));
	  		  }//endRun
	  	  }//end class printAudioLevel
	      
	  }//end class ServiceHandler

	  @Override
	  public void onCreate()
	  {
		  // Start up the thread running the service.  Note that we create a
		  // separate thread because the service normally runs in the process's
		  // main thread, which we don't want to block.  We also make it
		  // background priority so CPU-intensive work will not disrupt our UI.
		  thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
		  thread.start();
		  //thread.destroy();
		  //Toast.makeText(this, "service onCreate", Toast.LENGTH_SHORT).show();
		  // Get the HandlerThread's Looper and use it for our Handler 
		  mServiceLooper = thread.getLooper();
		  mServiceHandler = new ServiceHandler(mServiceLooper);
	  }//end onCreate

	  @Override
	  public int onStartCommand(Intent intent, int flags, int startId)
	  {
	      Toast.makeText(this, "Your favourite Sensor Personal Assistant at you service..", Toast.LENGTH_LONG).show();

	      // For each start request, send a message to start a job and deliver the
	      // start ID so we know which request we're stopping when we finish the job
	      Message msg = mServiceHandler.obtainMessage();
	      msg.arg1 = startId;
	      mServiceHandler.sendMessage(msg);
	      
	      // If we get killed, after returning from here, restart
	      return START_STICKY;
	  }//end onStartCommand

	  @Override
	  public IBinder onBind(Intent intent) {
	      // We don't provide binding, so return null
	      return null;
	  }
	  
	  @Override
	  public void onDestroy() {
		super.onDestroy();
		//thread.interrupt();
	    //Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show(); 
	  }
	}