package project.senseIt.start;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SenseItTrial extends Activity {
    /** Called when the activity is first created. */
 
	public static Activity activity;
	
	public static int index = 0;
	
	public static double shakeSensitivity=2.5;
	TextView ProximitySensor, ProximityMax, ProximityReading;
	TextView textView9;
	
	public static String customMsg = new String("Driving... Call U Later!");
	public static ArrayList<String> selectedContacts=new ArrayList<String>();
	public static ArrayList<String> selectedNos=new ArrayList<String>();
	public static double driveSpeed=30;
	SensorManager mySensorManager;
	Sensor myProximitySensor;
	SensorEventListener proximitySensorEventListener;
 
	LinearLayout ll = null;
 
	int ijk=9;
	public static String drive = new String("Stationary");
	
	private int color = 0;
	private View view;
	private long lastUpdate;
	TextView outputX;
	TextView outputY;
	TextView outputZ,output;
	Sensor mAccelerometer=null,mMagnetometer=null;
	Button b=null, b2 = null;
	//private boolean mLastAccelerometerSet = false;
	//private boolean mLastMagnetometerSet = false;
	
	MediaRecorder recorder=null;
	//for orientation values
	TextView outputX2;
	TextView outputY2;
	TextView outputZ2,light,audio;
	ImageView iv1;
	Map<Integer, Integer> map;	// = new Map<Integer, String>();
 
	
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
    	activity = this;
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main2);
        
        Intent i = getIntent();
        setResult(100, i);
        
        ll = (LinearLayout) findViewById(R.id.linear2);
        ll.setBackgroundResource(R.drawable.bg11);
        
        //Toast.makeText(getApplicationContext(), "onCreate", Toast.LENGTH_SHORT).show();
        
        //SparseIntArray maps = new SparseIntArray();
        //map = new HashMap<Integer, Integer>();
        //map.put(0, R.drawable.bg10);
        
        String info = new String("SenseIt is a Sensor based Personal Assistant.\n" +
        						  "Current Features:\n" +
        						  "As soon as user clicks on Start Service button, a service starts in the background which " +
        						  "acts upon based on sensor inputs. If phone's inbuilt music player is on, then user " +
        						  "can play/pause music by keeping hand near the proximity sensor for 2 seconds." +
        						  " A loud right or left shake of the phone in such a case plays" +
        						  " the next or the previous song of the playlist.\n" +
        						  "In case music player is not active, then a loud shake results" +
        						  " in switching between normal and silent sound profile.\n" +
        						  "OTHER EXCITING FEATURS TO BE ADDED VERY SHORTLY\n" +
        						  "You may stop the service by clicking on the 'Exit Application' button in the app" +
        						  " or by killing/stopping the app from manage application settings.");
        
        String driveOnOff = new String("SenseIt also affers a feature where the app detects using your GPS service" +
        		           " whether you are driving or not and then saves you from attending calls while driving by " +
        		           "automatically rejecting the call and sending an excuse me SMS 'I am driving. " +
        		           "Will call you later' to the caller. WOULD YOU LIKE TO START THIS FEATURE?");
        
        String aboutStart = new String("Just click on Start Service button. You may stop the service by " +
        							   "clicking on Exit Application button from the application interface or " +
        							   "by killing/stopping the application SenseIt from the " +
        							   "Manage Application Settings of your phone.\n" +
        							   "DO TRY IT ONCE :)");
        
        final Intent nextScreen = new Intent(getApplicationContext(), SenseItService.class);
        
        AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
        builder3.setMessage(aboutStart)
            .setCancelable(false)
            .setTitle("How to start")
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id)
                {
                	dialog.dismiss();
                	//driveQuery.show();
                }
            });
        final AlertDialog startInfo = builder3.create();
        //appInfo.show();
        
        AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
        builder2.setMessage(driveOnOff)
            .setCancelable(false)
            .setTitle("Let Me Drive")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id)
                {
                	drive="On";
                	dialog.dismiss();
                	startInfo.show();
                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id)
                {
                	drive="Off";
                	dialog.dismiss();
                	startInfo.show();
                }
            });
        final AlertDialog driveQuery = builder2.create();
        //driveQuery.show();
        
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(info)
            .setCancelable(false)
            .setTitle("About")
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id)
                {
                	dialog.dismiss();
                	driveQuery.show();
                }
            });
        final AlertDialog appInfo = builder.create();
        appInfo.show();
        
        b=(Button)findViewById(R.id.button1);
	    b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
            	stopService(nextScreen);
            	finish();
            	System.exit(0);
            }//end onClick
	    });//end new View.onClickListener
	    
	    
	    b2=(Button)findViewById(R.id.button2);
	    b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
				startService(nextScreen);
				finish();
            }//end onClick
	    });//end new View.onClickListener
        
	    SwipeGesture swipe = new SwipeGesture(getApplicationContext());
	    (findViewById(R.id.linear2)).setOnTouchListener(swipe);
	    
	    //Button btnNextScreen = (Button) findViewById(R.id.but1);
    	//Listening to button event
    	//btnNextScreen.setOnClickListener(new View.OnClickListener() {
        	//public void onClick(View arg0) {
            	//Starting a new Intent
            	//Intent nextScreen = new Intent(getApplicationContext(), ShakeItActivity.class);
            	//Sending data to another Activity
            	//nextScreen.putExtra("name", "ankit");
            	//nextScreen.putExtra("email", "iiitd");
            	//Log.e("n", inputName.getText()+"."+ inputEmail.getText());
            	//startActivityForResult(nextScreen, 100);     
        	//}
    	//});  
    }//end onCreate
    
    public static void saveSettings()
    {
    	try {
    		File f=new File(Environment.getExternalStorageDirectory().toString()+
    				"/SenseIt/data");
    		if(!f.exists())
    		{
    			f=new File(Environment.getExternalStorageDirectory().toString()+
    					"/SenseIt");
    			f.mkdir();
    			f=new File(Environment.getExternalStorageDirectory().toString()+
    					"/SenseIt/data");
    			f.createNewFile();
    		}
			PrintWriter out=new PrintWriter(new FileOutputStream(f));
			for(int i=0;i<selectedContacts.size();i++)
			{
				out.print(selectedContacts.get(i)+"-"+selectedNos.get(i)+"@");
			}
			out.print("!!!"+driveSpeed+"!!!");
			out.print(""+shakeSensitivity);
			out.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
    
    public static void getSettings()
    {
    	try {
			Scanner in=new Scanner(new FileInputStream(Environment.getExternalStorageDirectory().toString() +
					"/SenseIt/data"));
			String data=in.nextLine();
			String contacts[]=data.split("!!!")[0].split("@");
			shakeSensitivity=Double.parseDouble(data.split("!!!")[2]);
			driveSpeed=Double.parseDouble(data.split("!!!")[1]);
			for(int i=0;i<contacts.length;i++)
			{
				selectedContacts.add(contacts[i].split("-")[0]);
				selectedNos.add(contacts[i].split("-")[1]);
			}
			in.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
	  
    protected void onResume()
	{
    	super.onResume();
    	//Toast.makeText(getApplicationContext(), "onResume", Toast.LENGTH_SHORT).show();
    	/*mySensorManager.registerListener(proximitySensorEventListener, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mySensorManager.registerListener(proximitySensorEventListener, mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
	    mySensorManager.registerListener(proximitySensorEventListener, mySensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), mySensorManager.SENSOR_DELAY_GAME);
	    mySensorManager.registerListener(proximitySensorEventListener, mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),mySensorManager.SENSOR_DELAY_UI);
	    mySensorManager.registerListener(proximitySensorEventListener, mLight, SensorManager.SENSOR_DELAY_NORMAL);
		mySensorManager.registerListener(proximitySensorEventListener, myProximitySensor,SensorManager.SENSOR_DELAY_NORMAL);
		//textView9.setText("Resumed");*/
	}//end onResume
	
    
	/*class printAudioLevel extends TimerTask
	{
		private Handler handler=new Handler();
		  
		public void post(final String s)
		{
			final TextView tv=audio;
			handler.post(new Runnable() {
				public void run()
				{
					tv.setText(s);
		        }//endRun
		    });//end new Runnable
		}//end Post
		  
	    public void run()
		{
	    	if(recorder!=null)
	    		post("Noise Level(in db) : "+20*Math.log10(recorder.getMaxAmplitude()/7.5));
		}//endRun
	}//end class printAudioLevel*/
    
    /*protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    	super.onActivityResult(requestCode, resultCode, data);
    	Toast.makeText(getApplicationContext(), "OnActivityResult", Toast.LENGTH_SHORT).show();
    	if(resultCode == 100)
    	{
    		// Storing result in a variable called myvar
    		// get("website") 'website' is the key value result data
    		//String mywebsite = data.getExtras().get("result");
    		//ProximitySensor.setText("BAck");
    		//textView9.setText("NOISE");
    		onResume();
    	}//endIf
    }//endOnActivityResult*/
    
    protected void onPause()
   	{
       	super.onPause();
       	//saveState();
       	//onSaveInstanceState(savedInstanceState);
       	//Toast.makeText(getApplicationContext(), "onPause", Toast.LENGTH_SHORT).show();
   	}//end onPause
    
    protected void onStop()
   	{
       	super.onStop();
       	//Toast.makeText(getApplicationContext(), "onStop", Toast.LENGTH_SHORT).show();
   	}//end onStop
    
    protected void onDestroy()
   	{
       	super.onDestroy();
       	//Toast.makeText(getApplicationContext(), "onDestroy", Toast.LENGTH_SHORT).show();
   	}//end onDestroy
    
    protected void onStart()
   	{
       	super.onStart();
       	//Toast.makeText(getApplicationContext(), "onStart", Toast.LENGTH_SHORT).show();
   	}//end onStart
    
    protected void onRestart()
   	{
       	super.onDestroy();
       	//Toast.makeText(getApplicationContext(), "onRestart", Toast.LENGTH_SHORT).show();
   	}//end onRestart
    
    /*protected void onStartCommand()
   	{
       	super.onStartCommand();
       	Toast.makeText(getApplicationContext(), "onRestart", Toast.LENGTH_SHORT).show();
   	}//end onRestart*/
    
    /*protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    	super.onActivityResult(requestCode, resultCode, data);
    	Toast.makeText(getApplicationContext(), "OnActivityResult", Toast.LENGTH_SHORT).show();
    	if(resultCode == 100)
    	{
    		// Storing result in a variable called myvar
    		// get("website") 'website' is the key value result data
    		//String mywebsite = data.getExtras().get("result");
    		//ProximitySensor.setText("BAck");
    		//textView9.setText("NOISE");
    		//onResume();
    	}//endIf
    }//endOnActivityResult*/

}//end Class ShakeItActivity