package project.senseIt.start;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

//import senseIt.start.R;

//import senseIt.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SenseItActivity extends Activity {
    /** Called when the activity is first created. */
 
	public static Activity activity;
	
	TextView ProximitySensor, ProximityMax, ProximityReading;
	TextView textView9;
	
	SensorManager mySensorManager;
	Sensor myProximitySensor;
	SensorEventListener proximitySensorEventListener;
 
	LinearLayout ll = null;
 
	//int ijk=9;
	
	private int color = 0;
	private View view;
	private long lastUpdate;
	TextView outputX;
	TextView outputY;
	TextView outputZ,output;
	Sensor mAccelerometer=null,mMagnetometer=null;
	Button b=null;
	//private boolean mLastAccelerometerSet = false;
	//private boolean mLastMagnetometerSet = false;
	private Sensor mLight=null;
	private float currentLux,maxLux;
	private float mLastX, mLastY, mLastZ;
	private boolean mInitialized;
	private final float NOISE = (float) 2.0;
	MediaRecorder recorder=null;
	//for orientation values
	TextView outputX2;
	TextView outputY2;
	TextView outputZ2,light,audio;
	ImageView iv1;
	Map<Integer, Integer> map;	// = new Map<Integer, String>();
 
	
	/*public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }*/
	
	
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        activity = this;
        
        Intent i = getIntent();
        setResult(100, i);
        
        ll = (LinearLayout) findViewById(R.id.linear);
        ll.setBackgroundResource(R.drawable.bg11);
        
        Toast.makeText(getApplicationContext(), "onCreate", Toast.LENGTH_LONG).show();
        
        SparseIntArray maps = new SparseIntArray();
        map = new HashMap<Integer, Integer>();
        /*map.put(0, R.drawable.bg10);
        map.put(1, R.drawable.bg16);
        map.put(2, R.drawable.bg3);
        map.put(3, R.drawable.bg4);
        map.put(4, R.drawable.bg5);
        map.put(5, R.drawable.bg13);
        map.put(6, R.drawable.bg8);
        map.put(7, R.drawable.bg12);
        map.put(8, R.drawable.bg9);
        map.put(9, R.drawable.bg15);*/
        
        //iv1 = (ImageView) findViewById(R.id.imageView1);
        //iv1.setMaxHeight(1);
        //iv1.setMaxWidth(1);
        //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 100);
        //iv1.setLayoutParams(params);
        //iv1.setScaleType(ImageView.ScaleType.CENTER_CROP);
		
        
        //ProximitySensor = (TextView)findViewById(R.id.textView9);
        //ProximityMax = (TextView)findViewById(R.id.textView10);
        ProximityReading = (TextView)findViewById(R.id.textView11);
        
        mySensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        myProximitySensor = mySensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    
        lastUpdate = System.currentTimeMillis();        
        outputX = (TextView) findViewById(R.id.textView1);
        outputY = (TextView) findViewById(R.id.textView2);
        outputZ = (TextView) findViewById(R.id.textView3);
        //outputX2 = (TextView) findViewById(R.id.textView4);
        //outputY2 = (TextView) findViewById(R.id.textView5);
        //outputZ2 = (TextView) findViewById(R.id.textView6);
        output=(TextView) findViewById(R.id.textView7);
        light=(TextView) findViewById(R.id.textView8);
        mAccelerometer = mySensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mLight = mySensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        view = findViewById(R.id.textView7);
	    //view.setBackgroundColor(Color.BLACK);

	    TextView acc = (TextView) findViewById(R.id.acc);
	    TextView lgt = (TextView) findViewById(R.id.lgt);
	    TextView prx = (TextView) findViewById(R.id.prx);
	    TextView shk = (TextView) findViewById(R.id.shk);
	    audio=(TextView) findViewById(R.id.audio);
	    //audio.setText("NNOOIISSEE");
	    textView9 = (TextView) findViewById(R.id.textView9);
	    //textView9.setText(""+ijk);
	    
        proximitySensorEventListener = new SensorEventListener(){
        	@Override
        	public void onAccuracyChanged(Sensor sensor, int accuracy)
        	{
        		// TODO Auto-generated method stub
        	}//end onAccuracyChanged
        	
        	private void getAccelerometer(SensorEvent event)
        	{
    		    float[] values = event.values;
    		    // Movement
    		    float x = values[0];
    		    float y = values[1];
    		    float z = values[2];

    		    float accelationSquareRoot = (x * x + y * y + z * z)
    		        / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
    		    long actualTime = System.currentTimeMillis();
    		    if (accelationSquareRoot >= 2.5) //
    		    {
    		    	if (actualTime - lastUpdate < 500)
    		    	{
    		    		return;
    		    	}//end if
    		    	lastUpdate = actualTime;
    		    	//Toast.makeText(this, "Device was shuffed", Toast.LENGTH_SHORT).show();
    		    	if(Float.floatToIntBits(x)>8)
    		    	{	if(color==0)
    		    		color=map.size();
    		    		color = (color-1)%map.size();
    		    	}//end if
    		    	else
    		    		color = (color+1)%map.size();
    		    	ll.setBackgroundResource(map.get(color));
    		    }//endIf
    		  }//endGetAccelerometer

        	
        	//protected void onResume()
        	//{
        	    // Register a listener for the sensor.
        	    //super.onResume();
        	   // mySensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);
        		//myProximitySensor
            	//mySensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
                //mySensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
        	    //mySensorManager.registerListener(this, mySensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), mySensorManager.SENSOR_DELAY_GAME);
        	    //mySensorManager.registerListener(this, mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),mySensorManager.SENSOR_DELAY_UI);
        	    //mySensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_NORMAL);
        		//mySensorManager.registerListener(this, myProximitySensor,SensorManager.SENSOR_DELAY_NORMAL);
        	//}//end onResume
        	
        	
        	@Override
        	public void onSensorChanged(SensorEvent event)
        	{
        		// TODO Auto-generated method stub
        		if(event.sensor.getType()==Sensor.TYPE_PROXIMITY)
        		{
        			Toast.makeText(getApplicationContext(), "Proximity Changed", Toast.LENGTH_LONG).show();
        			if(String.valueOf(event.values[0]).equals("0.0"))
        			{
        				ProximityReading.setText("Proximity Sensor Reading: "+ "Near");
        				//onResume();
        				//Intent nextScreen = new Intent(getApplicationContext(), senseItActivity2.class);
        				//nextScreen.putExtra("name", "proxy");
        				//nextScreen.putExtra("email", "iiitd");
        				//startActivityForResult(nextScreen, 100);
        			}
        			else
        			{
        				ProximityReading.setText("Proximity Sensor Reading: "+ "Far");
        				//Intent nextScreen = new Intent(getApplicationContext(), senseItActivity2.class);
        				//nextScreen.putExtra("name", "proxy");
        				//nextScreen.putExtra("email", "iiitd");
        				//ijk=7;
        				//startActivityForResult(nextScreen, 100);
        				//onPause();
        			}
                    //Log.e("n", inputName.getText()+"."+ inputEmail.getText());
                    //mySensorManager.unregisterListener(this,myProximitySensor);   
        		}//end if
        		
        		if(event.sensor.getType()== Sensor.TYPE_LIGHT)
        	    {
        	        currentLux = event.values[0];
        	        if (currentLux > maxLux)
        	           maxLux = currentLux;
        	        light.setText("Sensed Light change : "+currentLux);
        	    }//end if
        		
        		if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
        		{
        	      getAccelerometer(event);
        	      //mySensorManager.unregisterListener(this,mAccelerometer);
        	    }//end if
        		        		
        	    synchronized (this)
        	    {
        	        switch(event.sensor.getType())
        	        {
        	            case Sensor.TYPE_ACCELEROMETER:
        	            {
        	            	float x = event.values[0];
        	            	float y = event.values[1];
        	            	float z = event.values[2];
        	            	if(!mInitialized)
        	            	{
        	            		mLastX = x;
        	            		mLastY = y;
        	            		mLastZ = z;
        	            		//outputX2.setText("0.0");
        	            		//outputY2.setText("0.0");
        	            		//outputZ2.setText("0.0");
        	            		mInitialized = true;
        	            	}// end if
        	            	else
        	            	{
        	            		float deltaX = Math.abs(mLastX - x);
        	            		float deltaY = Math.abs(mLastY - y);
        	            		float deltaZ = Math.abs(mLastZ - z);
        	            		if(deltaX < NOISE)
        	            			deltaX = (float)0.0;
        	            		if(deltaY < NOISE)
        	            			deltaY = (float)0.0;
        	            		if(deltaZ < NOISE)
        	            			deltaZ = (float)0.0;
        	            		mLastX = x;
        	            		mLastY = y;
        	            		mLastZ = z;
        	            		//outputX2.setText(Float.toString(deltaX));
        	            		//outputY2.setText(Float.toString(deltaY));
        	            		//outputZ2.setText(Float.toString(deltaZ));
        	            	}// end else
        	                  	            	
        	            	outputX.setText("x: "+Float.toString(event.values[0]));
        	                outputY.setText("y: "+Float.toString(event.values[1]));
        	                outputZ.setText("z: "+Float.toString(event.values[2]));
        	            	break;
        	            }// end case
        	            case Sensor.TYPE_MAGNETIC_FIELD:
        	            	//outputX2.setText("x:"+Float.toString(event.values[0]));
        	                //outputY2.setText("y:"+Float.toString(event.values[1]));
        	                //outputZ2.setText("z:"+Float.toString(event.values[2]));
        	            	break;
        	        }//endSwitch
        	    }//endSychronised        		
        	}//endOnSensorChanged
        };//endSensorEventListener
        
        if(myProximitySensor == null)
        {
        	ProximitySensor.setText("No Proximity Sensor!"); 
        }//end if
        else
        {
        	//ProximitySensor.setText(myProximitySensor.getName());
        	//ProximityMax.setText("Maximum Range: "+ String.valueOf(myProximitySensor.getMaximumRange()));
        	mySensorManager.registerListener(proximitySensorEventListener,myProximitySensor,SensorManager.SENSOR_DELAY_NORMAL);
        	mySensorManager.registerListener(proximitySensorEventListener, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
            mySensorManager.registerListener(proximitySensorEventListener, mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
    	    mySensorManager.registerListener(proximitySensorEventListener, mySensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), mySensorManager.SENSOR_DELAY_GAME);
    	    mySensorManager.registerListener(proximitySensorEventListener, mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),mySensorManager.SENSOR_DELAY_NORMAL);
    	    mySensorManager.registerListener(proximitySensorEventListener, mLight, SensorManager.SENSOR_DELAY_NORMAL);
        }//end else
        
        
        b=(Button)findViewById(R.id.button1);
	    b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
            	finish();
            	System.exit(0);
            }//end onClick
	    });//end new View.onClickListener
        
	    //Button btnNextScreen = (Button) findViewById(R.id.but1);
    	//Listening to button event
    	//btnNextScreen.setOnClickListener(new View.OnClickListener() {
        	//public void onClick(View arg0) {
            	//Starting a new Intent
            	//Intent nextScreen = new Intent(getApplicationContext(), senseItActivity.class);
            	//Sending data to another Activity
            	//nextScreen.putExtra("name", "ankit");
            	//nextScreen.putExtra("email", "iiitd");
            	//Log.e("n", inputName.getText()+"."+ inputEmail.getText());
            	//startActivityForResult(nextScreen, 100);     
        	//}
    	//});
	    recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile("/dev/null");
        try {
			recorder.prepare();
			recorder.start();
		} catch (Exception e){
			audio.setText(e.getMessage());
		}//end catch
	    
	    try{
	    	Timer t=new Timer();
	    	t.scheduleAtFixedRate(new printAudioLevel(), 0, 500);
	    }//end try
	    catch(Exception e)
	    {
	    	audio.setText(e.getMessage());
	    }//end catch
        //timer.scheduleAtFixedRate(new RecorderTask(recorder), 0, 1000);
        //rc.run();    
    }//end onCreate
    
	  
    /*protected void onResume()
	{
    	super.onResume();
    	Toast.makeText(getApplicationContext(), "onResume", Toast.LENGTH_LONG).show();
    	mySensorManager.registerListener(proximitySensorEventListener, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mySensorManager.registerListener(proximitySensorEventListener, mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
	    mySensorManager.registerListener(proximitySensorEventListener, mySensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), mySensorManager.SENSOR_DELAY_GAME);
	    mySensorManager.registerListener(proximitySensorEventListener, mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),mySensorManager.SENSOR_DELAY_UI);
	    mySensorManager.registerListener(proximitySensorEventListener, mLight, SensorManager.SENSOR_DELAY_NORMAL);
		mySensorManager.registerListener(proximitySensorEventListener, myProximitySensor,SensorManager.SENSOR_DELAY_NORMAL);
		//textView9.setText("Resumed");
	}//end onResume*/
	
    
	class printAudioLevel extends TimerTask
	{
		private Handler handler=new Handler();
		  
		public void post(final String s)
		{
			final TextView tv=audio;
			handler.post(new Runnable() {
				public void run()
				{
					tv.setText(s);
		        }//endRun
		    });//end new Runnable
		}//end Post
		  
	    public void run()
		{
	    	if(recorder!=null)
	    		post("Noise Level(in db) : "+20*Math.log10(recorder.getMaxAmplitude()/7.5));
		}//endRun
	}//end class printAudioLevel
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    	super.onActivityResult(requestCode, resultCode, data);
    	Toast.makeText(getApplicationContext(), "OnActivityResult", Toast.LENGTH_LONG).show();
    	if(resultCode == 100)
    	{
    		// Storing result in a variable called myvar
    		// get("website") 'website' is the key value result data
    		//String mywebsite = data.getExtras().get("result");
    		//ProximitySensor.setText("BAck");
    		//textView9.setText("NOISE");
    		onResume();
    	}//endIf
    }//endOnActivityResult
    
    protected void onPause()
   	{
       	super.onPause();
       	//saveState();
       	//onSaveInstanceState(savedInstanceState);
       	Toast.makeText(getApplicationContext(), "onPause", Toast.LENGTH_LONG).show();
   	}//end onPause
    
    protected void onStop()
   	{
       	super.onStop();
       	Toast.makeText(getApplicationContext(), "onStop", Toast.LENGTH_LONG).show();
   	}//end onStop
    
    protected void onDestroy()
   	{
       	super.onDestroy();
       	Toast.makeText(getApplicationContext(), "onDestroy", Toast.LENGTH_LONG).show();
   	}//end onDestroy
    
    protected void onStart()
   	{
       	super.onStart();
       	Toast.makeText(getApplicationContext(), "onStart", Toast.LENGTH_LONG).show();
   	}//end onStart
    
    protected void onRestart()
   	{
       	super.onDestroy();
       	Toast.makeText(getApplicationContext(), "onRestart", Toast.LENGTH_LONG).show();
   	}//end onRestart
    
    //protected void onStartCommand()
   	//{
    //   	super.onStartCommand();
    //   	Toast.makeText(getApplicationContext(), "onRestart", Toast.LENGTH_LONG).show();
   	//}//end onRestart

}//end Class senseItActivity