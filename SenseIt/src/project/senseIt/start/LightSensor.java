package project.senseIt.start;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

public class LightSensor {

	SensorEventListener sEventListener;
	SensorManager mySensorManager;
	Activity activity;
	private float currentLux,maxLux;
	Button b=null, b2=null;
	
	public LightSensor()
	{
		activity=SenseItTrial.activity;
		Animation anim=AnimationUtils.loadAnimation(activity.getApplicationContext()
				,R.anim.anim);
        activity.getWindow().getDecorView().startAnimation(anim);
		activity.setContentView(R.layout.light);
		final TextView desc = (TextView)activity.findViewById(R.id.lightDesc);
		desc.setText("Test Light Sensor of your phone");
		SwipeGesture swipe = new SwipeGesture(activity.getApplicationContext());
        (activity.findViewById(R.id.lightLayout)).setOnTouchListener(swipe);
        
        mySensorManager = (SensorManager)activity.getSystemService(Context.SENSOR_SERVICE);
        
        final Intent nextScreen = new Intent(activity, SenseItService.class);
        
        b=(Button)activity.findViewById(R.id.lightbutton2);
	    b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
            	activity.stopService(nextScreen);
            	activity.finish();
            	System.exit(0);
            }//end onClick
	    });//end new View.onClickListener
	    
	    
	    b2=(Button)activity.findViewById(R.id.lightbutton1);
	    b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
				activity.startService(nextScreen);
				activity.finish();
            }//end onClick
	    });//end new View.onClickListener

        
        sEventListener = new SensorEventListener(){
        	@Override
        	public void onAccuracyChanged(Sensor sensor, int accuracy) {
        		// TODO Auto-generated method stub
        	}
        	
        	protected void onResume() {
        	/////
        		 mySensorManager.registerListener(this, mySensorManager.getDefaultSensor(Sensor.TYPE_LIGHT), SensorManager.SENSOR_DELAY_NORMAL);
        	}
        	
        	@Override
        	public void onSensorChanged(SensorEvent event) {
        		if(event.sensor.getType()== Sensor.TYPE_LIGHT)
        	    {
        	        currentLux = event.values[0];
        	        if (currentLux > maxLux)
        	           maxLux = currentLux;
        	        desc.setText("Sensed Light change : "+currentLux);
        	    }

        		
        	}

        };
        mySensorManager.registerListener(sEventListener,mySensorManager.getDefaultSensor(
        		Sensor.TYPE_LIGHT) , SensorManager.SENSOR_DELAY_NORMAL);
        ///mySensorManager.registerListener(sEventListener,,SensorManager.SENSOR_DELAY_NORMAL);
        
	}
}
