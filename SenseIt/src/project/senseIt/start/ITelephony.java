package project.senseIt.start;

public interface ITelephony {
	
	boolean endCall();
    void answerRingingCall();
    void silenceRinger();
}
