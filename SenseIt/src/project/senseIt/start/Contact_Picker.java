package project.senseIt.start;

import java.util.ArrayList;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class Contact_Picker extends Activity {

    protected String TAG = null;
    public String[] Contacts = {};
    public int[] to = {};
    public ListView myListView;
    ListAdapter adapter;
    ArrayList<String> selected=new ArrayList<String>();
    /** Called when the activity is first created. */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.contacts);

            final Button done_Button = (Button) findViewById(R.id.button1);
            //final Button clear_Button =(Button) findViewById(R.id.clear_Button);

            //Cursor mCursor = getContacts();
            //Cursor mCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
            Cursor mCursor=getContacts();
            String contactName = null;
            ArrayList<String> phones=new ArrayList<String>();
            while (mCursor.moveToNext()) 
            {
            	
            	//phones.clear();
            	/*int len=Integer.parseInt(mCursor.getString(mCursor
                        .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
            		*/
            	contactName  = mCursor.getString(mCursor.getColumnIndex(
            			ContactsContract.Contacts.DISPLAY_NAME));
                	//for(int k=0;k<len;k++) 
                    {
                		//phones.add(contactName);
                		//mCursor.moveToNext();
                    }
                selected.add(contactName);
                //AndroidProg.tv.append("\n"+contactName);
            }
            startManagingCursor(mCursor);

            adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_multiple_choice, mCursor,
            Contacts = new String[] {ContactsContract.Contacts.DISPLAY_NAME },
            to = new int[] { android.R.id.text1 });
            myListView = (ListView)findViewById(R.id.list);
            myListView.setAdapter(adapter);
            
            myListView.setItemsCanFocus(false);
            myListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            
            /*for(int i=0;i<selected.size();i++)
            {
            	AndroidProg.tv.append(selected.get(i)+"\n");
            }*/
           
            
            /*clear_Button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(),"Selections Cleared", Toast.LENGTH_SHORT).show();
                    ClearSelections();
                }
            });*/

            /** When 'Done' Button Pushed: **/  		
            done_Button.setOnClickListener(new View.OnClickListener() {
                public void onClick (View v){
                    SparseBooleanArray selectedPositions = myListView.getCheckedItemPositions();

                    for (int i=0; i<myListView.getCount(); i++) {
                        if (selectedPositions.get(i) == true) {
                        	SenseItTrial.selectedContacts.add(selected.get(i));
                        	
                        	//AndroidProg.tv.append("\n"+selected.get(i));
                        }
                        
                    }
                    finish();
                    Drive.Contacts();
                    /*Log.i(TAG,"Number of Checked Positions: " + checkedPositions.size());
                    if (checkedPositions != null)
                    {
                        int count = myListView.getCount();
                        for ( int i=0;i<count;i++)
                        {
                            Log.i(TAG,"Selected items: " + checkedPositions.get(i));
                        }
                    }*/

                }
            }); //<-- End of Done_Button

        } //<-- end of onCreate();
        
        


        private void ClearSelections() {            

            int count = this.myListView.getAdapter().getCount();

            for (int i = 0; i < count; i++) {
                this.myListView.setItemChecked(i, false);
            }

        }

        private Cursor getContacts() {
            // Run query
            Uri uri = ContactsContract.Contacts.CONTENT_URI;
            String[] projection = new String[] { ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME };
            String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '"
                    + ("1") + "'";
            String[] selectionArgs = null;
            String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
                    + " COLLATE LOCALIZED ASC";

            return managedQuery(uri, projection, selection, selectionArgs,
                    sortOrder);
        } 
}
