package project.senseIt.start;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnTouchListener;

public class SwipeGesture extends SimpleOnGestureListener implements OnTouchListener{
        
	Context context;
	GestureDetector gDetector;
	public SwipeGesture()
	{
		super();
	}

	public SwipeGesture(Context context) {
		this(context, null);
	}

	public SwipeGesture(Context context, GestureDetector gDetector) {

		if(gDetector == null)
			gDetector = new GestureDetector(context, this);

		this.context = context;
		this.gDetector = gDetector;
	}

	float prev=0;
	public boolean onTouch(View v, MotionEvent event) {
		//tv.append("Moved"+"\n"+event.getX());
		synchronized (event)
		{
			if(event.getAction()==MotionEvent.ACTION_UP)
			{
				float diff=prev-event.getX();
				if(diff<-60)
				{
					SenseItTrial.index--;
					if(SenseItTrial.index<0)
					{
						SenseItTrial.index=3;
					}
				}
				else if(diff>60)
				{
					SenseItTrial.index++;
					if(SenseItTrial.index>3)
					{
						SenseItTrial.index=0;
					}
				}
				switch(SenseItTrial.index)
				{
					case 0:
					{
						LightSensor LS=new LightSensor();
						break;
					}
					case 1:
					{
						ShakeTester SG=new ShakeTester();
						break;
					}
					case 2:
					{
						ProximityTester SG=new ProximityTester();
						break;
					}
					case 3:
					{
						Drive d=new Drive();
						break;
					}
				}

			}
			if(event.getAction()==MotionEvent.ACTION_DOWN)
			{
				prev=event.getX();
			}
		}
		return true;
	}


	public GestureDetector getDetector()
	{
		return gDetector;
	}       
}
