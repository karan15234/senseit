package project.senseIt.start;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class Drive {

	Activity activity;
	public int Progress=0;
	public static TextView desc;
	Button b=null, b2=null;
	
	public Drive()
	{
		activity=SenseItTrial.activity;
		activity.setContentView(R.layout.drive);
		desc = (TextView)activity.findViewById(R.id.driveDesc);
		desc.setText("Set the speed of your car at which Call Rejection Mode" +
				"will be Activated");
		Animation animation = AnimationUtils.loadAnimation(activity.getBaseContext(),R.anim.slide_right_in);
		animation.setStartOffset(0);
		SwipeGesture swipe = new SwipeGesture(activity.getApplicationContext());
		final RelativeLayout layout=(RelativeLayout) activity.findViewById(R.id.driveLayout);
        layout.setOnTouchListener(swipe);
        layout.startAnimation(animation);
        
        final SeekBar driveSeek = (SeekBar)activity.findViewById(R.id.driveBar);
        driveSeek.setProgress((int)SenseItTrial.driveSpeed);
        driveSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
           
            public void onStopTrackingTouch(SeekBar arg0) {
            	SenseItTrial.driveSpeed=5.0+(float)(Progress*95)/100;
            	//desc.append(" "+sensitivity);
            }
            
            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }
            
            @Override
            public void onProgressChanged(SeekBar bar, int val, boolean arg2) {
                driveSeek.setProgress(val);
                Progress=val;
            }
        });
      
        
        final Intent nextScreen = new Intent(activity, SenseItService.class);
        
        b=(Button)activity.findViewById(R.id.drivebutton2);
	    b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
            	activity.stopService(nextScreen);
            	activity.finish();
            	System.exit(0);
            }//end onClick
	    });//end new View.onClickListener
	    
	    
	    b2=(Button)activity.findViewById(R.id.drivebutton1);
	    b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
				activity.startService(nextScreen);
				activity.finish();
            }//end onClick
	    });//end new View.onClickListener
	    
	    Button choose=(Button)activity.findViewById(R.id.choose);
	    choose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
            	activity.startActivity(new Intent(activity.getApplicationContext(), 
            			Contact_Picker.class));
            }
	    });
	    
	}
	
	public static void Contacts()
    {
    	for(int i=0;i<SenseItTrial.selectedContacts.size();i++)
    	{
    		String no=getPhoneNumber(SenseItTrial.selectedContacts.get(i));
    		//desc.append("\n"+SenseItTrial.selectedContacts.get(i)+" : "+no);
    		SenseItTrial.selectedNos.add(no);
    	}
    }
    
    public static String getPhoneNumber(String name) {
    	String ret = null;
    	String selection = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" like'%" + name +"%'";
    	String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.NUMBER};
    	Cursor c = SenseItTrial.activity.getApplicationContext().getContentResolver().query(ContactsContract.
    			CommonDataKinds.Phone.CONTENT_URI,
    	        projection, selection, null, null);
    	if (c.moveToFirst()) {
    	    ret = c.getString(0);
    	}
    	c.close();
    	if(ret==null)
    	    ret = "Unsaved";
    	return ret;
    	}
}
